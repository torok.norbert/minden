import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import allReducers from './reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';

//az applicaton state  +  reducers  = store

//const createMyStore = applyMiddleware()(createStore);    

const sagaMiddleware = createSagaMiddleware();

const  store = createStore( allReducers,  applyMiddleware(sagaMiddleware) ); 
sagaMiddleware.run(rootSaga);
 

ReactDOM.render(
  <React.StrictMode>
    <Provider store = {store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
//a createmystore fgv letrehozza nekem az alkalmazas store-jat amelyet exportalhatok mas allomanyba
//store = appstate + reducers
//Pl. fuggveny_A() actioncreator fuggvenyem,meghivhatom:
//store.dispatch(fuggveny_A)
//a dispatch adja at a reducereknek, nelkule nem kerulnek at

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
