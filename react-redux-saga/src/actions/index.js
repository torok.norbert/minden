export function kivalasztottVaros (cityName){
    return {
        type: 'CITY_SELECTED',
        payload: cityName
    };
}

//kotelezo modon object-et ad vissza, amelynek legalabb egyik kulcsa adott(type)
//a visszaadott object-et nevezzuk action-nek


export function cityWeather (cityName){
    console.log(`Action creator: cityweathe with param: ${cityName}`)
    return {
        type: 'CITY_SELECTED_WEATHER_REQUEST',
        payload: cityName
    };
}
