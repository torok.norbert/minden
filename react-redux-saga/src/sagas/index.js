import {takeLatest, takeEvery, all  } from 'redux-saga/effects';
import handleRquestedCityWeatherAsync from './handleRquestedCityWeatherAsync.saga';

export default function* rootsaga () { 
    yield all([
        takeEvery('CITY_SELECTED_WEATHER_REQUEST',handleRquestedCityWeatherAsync)
    ]);
};

