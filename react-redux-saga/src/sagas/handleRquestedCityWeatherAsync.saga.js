import { delay, call, put   } from 'redux-saga/effects';

const url = `http://api.openweathermap.org/data/2.5/weather?appid=9897e7359ac1de0f60cc20bfc64768d9&`;

export default function * (action){
    const updatedUrl = `${url}q=${action.payload}&units=metric`
    let data = yield call(() => fetch(updatedUrl) 
                            .then(response =>  response.json()));

    yield put({
        type: 'WEATHER_RESULT_OK',
        payload: data
    }) 
}