export default function(appState = null,action){
    switch(action.type){
        case 'CITY_SELECTED_WEATHER_REQUEST':
            return action.payload;
        
        default:
            return appState;
    }
}