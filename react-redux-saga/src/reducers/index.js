import getCityList from './CityList.reducer';
import selectedCity from './selectedCityReducer'
import {combineReducers} from 'redux';
import cityWeatherOK from './FetchWeather.reducer'

//a következő sorokban megadom, hogy az application state mindegyik kulcsáért melyik reducer felel

const rooteReducer = combineReducers({
    cityList: getCityList,
    selectedCity: selectedCity,
    cityWeatherOK: cityWeatherOK //ha a ketto egyforma akkor eleg az egyik is
});

export default rooteReducer;
