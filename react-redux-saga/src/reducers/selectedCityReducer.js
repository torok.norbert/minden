export default function(appState = null,action){
    switch(action.type){
        case 'SELECTED_CITY':
            console.log('selectedCityReducer');
            console.log(`appstate: ${appState}`)
            return action.payload;
        
        default:
            return appState;
    }
}