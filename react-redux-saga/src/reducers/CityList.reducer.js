export default function () {
    return [
        {name: "Nagyvárad"},
        {name: "Kolozsvár"},
        {name: "Kispetri"},
        {name: "Udvarhely"},
        {name: "Gyergyó"},
        {name: "Sámson"},
        {name: "Barót"},
        {name: "Zilah"},
        {name: "Szentháromság"},
    ];
}