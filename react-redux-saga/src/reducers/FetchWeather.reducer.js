export default function(appState = null,action){
    switch(action.type){
        case 'WEATHER_RESULT_OK':
            console.log(`cityWeatherOK reducer,action = ${JSON.stringify(action)} `);
            return action.payload.main.temp;
        
        default:
            return appState;
    }
}