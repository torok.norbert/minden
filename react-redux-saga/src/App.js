import React from 'react';
import './App.css';
import CityList from './containers/CityList.container';
import SelectedCity from './containers/SelectedCity.container';
import CityInput from './containers/CityInput.container'
import Weather from './containers/Weather.container'

class App extends React.Component{
  render(){
    return(
      <div>
        <header>
          <h2>Példa middleware használata</h2>
          <CityInput>
          </CityInput>
          <Weather></Weather>
        </header>
      </div>
    )
  } 
}

export default App;
