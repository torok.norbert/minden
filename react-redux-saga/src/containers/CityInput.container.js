import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { cityWeather } from '../actions';


class CityInput extends Component{

    constructor(props) {
        super(props);
    
        this.state = { helysegNev: "" };
 
      }
    
      onInputChange = (event) => {
        this.setState({ helysegNev: event.target.value });
      }
    
      onFormSubmit = (event) => {
        event.preventDefault();
        this.props.getWeather(this.state.helysegNev);
        this.setState({ helysegNev: "" });
      }
    

    render(){
        return(
            <form onSubmit={this.onFormSubmit} >
                <input
                        placeholder="Idojaras elorejelzes varosra"
                        value={this.state.helysegNev}
                        onChange={this.onInputChange}
                 />
                <span>
                    <button type="submit">Submit</button>
                </span>
             </form>


        );
    }
}


function bindDispatchToProps(dispatch){
    return bindActionCreators({getWeather: cityWeather},dispatch);
}

export default connect(null,bindDispatchToProps)(CityInput);    