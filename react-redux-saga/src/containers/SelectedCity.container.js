import React, { Component } from 'react';
import { connect } from 'react-redux';



class SelectedCity extends Component{
    render(){
        if(this.props.propsCitiesList){
                    return (
                        <div>Selected city:
                            {this.props.propsCitiesList}
                            </div>

        );}

        return(
            <div>
                No city selected yet
            </div>
        );
    }
}

function bindAppStateToProps(appState){        //tetszőleges név 
    return {
        propsCitiesList: appState.selectedCity   //indexből a kulcs
    };
}

export default connect(bindAppStateToProps)(SelectedCity)
