import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {kivalasztottVaros} from '../actions'

class CityList extends React.Component{
renderCityList(){
    return (
        <ul>
            {this.props.propsCitiesList.map((city) =>{
                        return (
                            <li 
                                onClick = {() =>{this.props.propsSelectedCity(city.name)}}
                                key = {city.name} >

                                {city.name}
                            </li>)
                    })}
        </ul>
    )
}

    render(){
        return(
            <div>
                <p>
                    A varosok listaja:
                </p>
                {this.renderCityList()}
            </div>
        );
    }
}
//a fuggvenynev tetszoleges
//celja appstore egy szeletet egy helyi propshoz kotni
function bindAppStateToProps(appState){        //tetszőleges név 
    return {
        propsCitiesList: appState.cityList     //indexből a kulcs
    };
}

//a fuggvenynev tetszoleges
//celja helyi propsot egyk kulso fuggvenyhez kotni

function bindDispatchToProps(dispatch){
    return bindActionCreators({propsSelectedCity: kivalasztottVaros},dispatch);
}


export default connect(bindAppStateToProps,bindDispatchToProps)(CityList);      //appstate egyik darabjt összekapcsolja a komponenesem egyik propszával, impliciten, nem kell megnevvezzem
